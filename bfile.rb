#!/usr/bin/env ruby

require 'rubygems'
require 'sinatra'
require 'haml'
require 'logger'

require './check_excel'

if !File.directory?("tmp")
  FileUtils.mkdir('tmp')
end
logger = Logger.new("tmp/pc.log")
$pwd = ENV['PWD']

def file_listing(directory)
  Dir.glob(directory + '/tmp/*.csv')
end

get '/upload' do
  haml :upload
end

post '/upload' do
  file = params[:file]
  filename = file[:filename]
  tempfile = file[:tempfile]

  ext = filename.split('.')[-1]
  if !['xls',  'xlsx'].include?(ext)
    redirect '/browse'
    return
  end

  _filename = "check_status.#{ext}"
  File.open("tmp/#{_filename}", 'w') {|f| f.write tempfile.read}
  begin
    Thread.new do |thread|
      pc  = ParseAndCheckExcel.new("tmp/#{_filename}")
      pc.parse_file
      pc.check
      puts pc.finished
      pc.save_as_csv
    end
  rescue Exception => e
    logger.fatal(message)
  end

  # remove old csv to save space
  FileUtils.rm(Dir["tmp/*.csv"])

  redirect '/browse'
end

get '/download/:filename' do |filename|
  file = File.join($pwd, "tmp", filename)
  send_file(file, :disposition => 'attachment')
end

get '/browse' do
  @files = file_listing($pwd)
  haml :index
end

#get '/bfile.rb' do
#  file = File.join($pwd, 'bfile.rb')
#  send_file(file, :disposition => 'attachment')
#end

get '/' do
  if $download_file
    file = File.join($pwd, $download_file)
    send_file(file, :disposition => 'attachment')
  else
    redirect '/browse'
  end
end

__END__

@@ layout
%html
  %a{:href => '/browse'}Browse Files
  %a{:href => '/upload'}Upload a File
  = yield

@@ index
%h1 File Server
%table
  %tr
    %th File
    %th Size
  - for file in @files
    - if File.file?(file)
      %tr
        %td
          %a{:title => file, :href => '/download/' + File.basename(file)}=File.basename(file)
        %td= File.size(file).to_s + "b"

@@upload
%h1 Upload

%form{:action=>"/upload",:method=>'post',:enctype=>"multipart/form-data"}
  %input{:type => "file",:name => "file"}
  %input{:type => "submit",:value => "Upload"}
