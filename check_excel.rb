require 'roo'
require 'httpclient'
require 'csv'
require 'thread'

THREAD_COUNT = 8  # tweak this number for maximum performance.

PARSE_RULES = {
  "sooxie": '/html/body/form[1]/div[7]/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/strong[2]/font',
  "k3": '/html/body/div[9]/div[1]/div[4]/div[2]/p[8]/a',
}

SLEEPS = Array(5..15)

class ParseAndCheckExcel
  attr_accessor :finished
  attr_reader :clt

  def initialize(filename)
    @filename = filename
    @rows = []
    @finished = false
    @clt = HTTPClient.new
  end

  def parse_file()
    puts "start parsing"
    print "filename is ", @filename, "\n"

    xlsx = ::Roo::Spreadsheet.open(@filename, extension: :xlsx)
    sheet = xlsx.sheet(0)

    (sheet.first_row..sheet.last_row).each_with_object(@rows) do |idx, obj|
      row = sheet.row(idx)
      next if row[0].nil?
      
      row_hash = {
        sku: row[0],
        info: row[1],
        price: row[2],
        sp: row[3],
        url: row[4],
      }
      obj << row_hash
    end
    puts "end parsing ........."
  end

  def set_rows(values)
    @rows = values
  end

  def check_status(url)
    return 'not_a_url' unless url.start_with?('http')
    begin
      res = clt.get(url)
      print "url is ", url, " res.code is ", res.code, "\n"
      if String(res.code) =~ /3\d\d/
        'not_found'
      elsif String(res.code) =~ /4\d\d/
        'access_blocked'
      elsif String(res.code) =~ /2\d\d/
        doc = Nokogiri::HTML(res.body)
        if (url.include?("k3.cn"))
          dom = doc.css('a.button:nth-child(1)').text
        else
          rule = PARSE_RULES[:sooxie]
          dom = doc.xpath(rule).text
        end

        if dom =~ /已下架/
          'down'
        end
      else 
        "请求出错"
      end
    rescue Exception => e
      puts 'timeout......'
      'timeout'
    end
  end

  def check()    
    res = []
    mutex = Mutex.new
    THREAD_COUNT.times.map {
      Thread.new(@rows, res) do |infos, res|
        while info = mutex.synchronize { infos.pop }
          sleep SLEEPS.sample

          status = check_status(info[:url])
          mutex.synchronize { info[:status] = status; res << info }
        end
      end
    }.each(&:join)

    set_rows(res)

    @finished = true
  end
=begin
    @rows.each do |info|
      info[:status] = check_status(info[:url])
    end

    @finished = true
  end
=end

  def get_rows
    @rows
  end

  def save_as_csv
    return  if !finished
    csv_content = CSV.generate() do |csv|
      csv << ["sku", "info", "price", "sp", "url", "status"]
      get_rows.each do |row|
        csv << [row[:sku], row[:info], row[:price], row[:sp], row[:url], row[:status]]
      end
    end
    File.open("tmp/#{ Time.now.strftime "%Y-%m-%d-%H-%M-%S"}-verified.csv", "w"){|file| file.write csv_content }
  end
end